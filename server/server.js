var mysql = require("mysql");
var express = require("express");
var session = require("express-session");
var app = express();
var cors = require("cors");

var bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

app.use(
  session({
    secret: "ssshhhhh",
    saveUninitialized: true, // (default: true)
    resave: true
  })
);

app.use(cors({ origin: ["http://localhost:3000"], credentials: true }));

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "fitness_app"
});

con.connect(err => {
  if (err) throw err;
  console.log("Database connected -> ");
});

var server = app.listen(8090, () => {
  var host = "localhost";
  var port = server.address().port;
  console.log("App listening at http://%s:%s", host, port);
});

const validatePayloadMiddleware = (req, res, next) => {
  if (req.body) {
    next();
  } else {
    res.status(403).send({
      errorMessage: "You need a payload"
    });
  }
};

app.post("/api/login", validatePayloadMiddleware, (req, res) => {
  con.query(
    "SELECT email, password FROM `user_information` WHERE email='" +
      req.body.email +
      "' AND password='" +
      req.body.password +
      "'",
    (err, result) => {
      if (err) throw err;
      if (result && result[0].password === req.body.password) {
        req.session.user = { email: req.body.email };
        req.session.save();
        res.status(200).send({ email: req.body.email });
      } else {
        res.status(403).send({
          errorMessage: "Permission denied!"
        });
      }
    }
  );
});

app.get("/api/login", (req, res) => {
  req.session.user
    ? res.status(200).send({ loggedIn: true })
    : res.status(200).send({ loggedIn: false });
});

app.post("/api/register", (req, res) => {
    con.query(
      "INSERT INTO `user_information` (`firstname`, `lastname`, `email`, `password`, `weightInKg`, `heightInCm`) VALUES('" +
        req.body.firstname +
        "','" +
        req.body.lastname +
        "','" +
        req.body.email +
        "','" +
        req.body.password +
        "','" +
        req.body.weight +
        "','" +
        req.body.height +
        "')",
      (err, result) => {
        if (err) throw err;
        res.send(JSON.stringify(result));
      }
    );
  });

app.post("/api/addImage", (req, res) => {
    con.query("INSERT INTO `process_images` (`uid`, `imagepath`) VALUES('"+req.body.uid+"', '"+req.body.imagepath+"')",
        (err, result) => {
            if (err) throw err;
            res.send(JSON.stringify(result));
        }
    );
});

app.get("/api/getImages", (req, res) => {
    let email = req.session.user.email;
    con.query(
      "SELECT uid FROM `user_information` WHERE email='" + email + "'",
      (err, userInfo) => {
        con.query(
          "SELECT * FROM `process_images` WHERE uid ='" +
            userInfo[0].uid +
            "'",
          (err, result) => {
            if (err) throw err;
            res.send(JSON.stringify(result));
          }
        );
      }
    ); 
});

app.post("/api/logout", (req, res) => {
  req.session.destroy(err => {
    if (err) {
      res.status(500).send("Could not log out.");
    } else {
      res.status(200).send({});
    }
  });
});