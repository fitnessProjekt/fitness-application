const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const uuid = require('uuid');
const jwt = require('jsonwebtoken');
const db = require('../lib/db.js');
const userMiddleware = require('../middleware/users.js');

router.post('/register', userMiddleware.validateRegister, (req, res, next) => {
    db.query(
      `SELECT * FROM useraccount WHERE LOWER(username) = LOWER(${db.escape(
        req.body.username
      )});`,
      (err, result) => {
        if (result.length) {
          return res.status(409).send({
            msg: 'Benutzername Bereits verwendet!'
          });
        } else {
          // username is available
          bcrypt.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).send({
                msg: err
              });
            } else {
              // has hashed pw => add to database  
              db.query(
                `INSERT INTO useraccount (username, userpw) VALUES (${db.escape(
                  req.body.username
                )}, ${db.escape(hash)})`,
                (err, result) => {
                  if (err) {
                    throw err;
                    return res.status(400).send({
                      msg: err
                    });
                  }
                  // return res.status(201).send({
                  //   msg: 'Regestriert!'
                  // });
                }
              );

              db.query(
                `SELECT id FROM useraccount WHERE username =  ${db.escape(req.body.username)}`,
                (err, result) => {
                
                  db.query(
                    `INSERT INTO user_information (uid, firstname, lastname, weightInKg, heightInCm, age, sportProWeek, gender) VALUES 
                    (${db.escape(result[0].id)}, ${db.escape(req.body.firstname)}, ${db.escape(req.body.lastname)}, ${db.escape(req.body.weightInKg)}, ${db.escape(req.body.heightInCm)}, ${db.escape(req.body.age)}, ${db.escape(req.body.sportProWeek)}, ${db.escape(req.body.gender)})`,
                    (err, result) => {
                      if (err) {
                        throw err;
                        return res.status(400).send({
                          msg: err
                        });
                      }
                      return res.status(201).send({
                        msg: 'Regestriert!'
                      });
                    }
                  )
                }
              );
              
            }
          });
        }
      }
    );
  });


  router.post('/profileRefresh/', (req, res) => {
        db.query(
          `UPDATE user_information SET 
          firstname = ${db.escape(req.body.firstname)},
          lastname = ${db.escape(req.body.lastname)},
          weightInKg=${db.escape(req.body.weightInKg)},
          heightInCm = ${db.escape(req.body.heightInCm)},
          age=${db.escape(req.body.age)},
          sportProWeek=${db.escape(req.body.sportProWeek)},

          gender=${db.escape(req.body.gender)} WHERE uid = ${db.escape(req.body.uid)}`,
          (err, result) => {
            if (err) {
              throw err;
              return res.status(400).send({
                msg: err
              });
            }
            return res.status(201).send({
              msg: 'Aktualisiert'
            });
          }
        )
      
  });



  router.get('/getProfile/:username', (req, res) => {
    db.query(
      `SELECT id FROM useraccount WHERE username =  ${db.escape(req.params.username)}`,
      (err, result) => {
        db.query(
          "SELECT uid, firstname, lastname, weightInKg, heightInCm, age, sportProWeek, profilePic, gender FROM `user_information` WHERE uid='" +
          result[0].id +
          "'",
          (err, result) => {
            if (err) throw err;
            res.send(result);
          }
        )
      })
  });






router.post('/login', (req, res, next) => {
  db.query(
    `SELECT * FROM useraccount WHERE username = ${db.escape(req.body.username)};`,
    (err, result) => {
      // user does not exists
      if (err) {
        throw err;
        return res.status(400).send({
          msg: err
        });
      }
      if (!result.length) {
        return res.status(401).send({
          msg: 'Benutzer oder Passwort falsch'
        });
      }
      bcrypt.compare(
        req.body.password,  
        result[0]['userpw'],
        (bErr, bResult) => {
          // wrong password
          if (bErr) {
            throw bErr;
            return res.status(401).send({
              msg: 'Benutzer oder Passwort falsch'
            });
          }
          if (bResult) {
            const token = jwt.sign({
                username: result[0].username,
                userId: result[0].id
              },
              'shhhhh', {
                expiresIn: '7d'
              }
            );
            return res.status(200).send({
              msg: 'Logged in!',
              token,
              user: result[0]
            });
          }
          return res.status(401).send({
            msg: 'Benutzer oder Passwort falsch',
          });
        }
      );
    }
  );
});



router.get('/secret-route', userMiddleware.isLoggedIn, (req, res, next) => {
  console.log(req);
  res.send('This is the secret content. Only logged in users can see that!');
});

router.get('/getExercises', (req, res) => {
  db.query(
    `SELECT * FROM exercises LIMIT 8`,
    (err, result) => {
      if (err) throw err;
      res.send(result);
    })
});


module.exports = router;