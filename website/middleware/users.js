module.exports = {
    validateRegister: (req, res, next) => {

      if (!req.body.gender) {
        return res.status(400).send({
          msg: 'Geschlecht'
        });
      }
      if (!req.body.firstname) {
        return res.status(400).send({
          msg: 'Vorname'
        });
      }
      if (!req.body.lastname) {
        return res.status(400).send({
          msg: 'Name'
        });
      }
      if (!req.body.weightInKg) {
        return res.status(400).send({
          msg: 'Gewicht'
        });
      }
      if (!req.body.heightInCm) {
        return res.status(400).send({
          msg: 'Gewicht'
        });
      }
      if (!req.body.age) {
        return res.status(400).send({
          msg: 'Alter'
        });
      }
      if (!req.body.sportProWeek) {
        return res.status(400).send({
          msg: 'Sport pro Woche'
        });
      }

      // username min length 3
      if (!req.body.username || req.body.username.length < 3) {
        return res.status(400).send({
          msg: 'Der Benutzername muss mindestens 3 Zeichen lang sein'
        });
      }
  
      // password min 6 chars
      if (!req.body.password || req.body.password.length < 6) {
        return res.status(400).send({
          msg: 'Das Passwort muss mindestens aus 6 Zeichen bestehen'
        });
      }
  
      // password (repeat) does not match
      if (
        !req.body.password_repeat ||
        req.body.password != req.body.password_repeat
      ) {
        return res.status(400).send({
          msg: 'Die Passwörter sind nicht gleich'
        });
      }
  
      next();
    },
    isLoggedIn: (req, res, next) => {
        try {
          const token = req.headers.authorization.split(' ')[1];
          const decoded = jwt.verify(
            token,
            'shhhhh'
          );
          req.userData = decoded;
          next();
        } catch (err) {
          return res.status(401).send({
            msg: 'Bitte Melden Sie sich erneut an!'
          });
        }
      }
  };